# -*- coding: utf-8 -*-
from __future__ import division, print_function  # remove when py3

import gzip
import subprocess
import sys
from multiprocessing import cpu_count

from typing import Tuple

from py3.py3pathlib import Path

CPU_COUNT = cpu_count()


def run_fastqc(input_file, output_dir, cpu_count=CPU_COUNT):  # type: (Path, Path, int) -> subprocess.Popen
    command = ['fastqc', str(input_file), '-o', str(output_dir), '-t', str(cpu_count)]
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def run_flash(output_folder, min_overlap, max_overlap, input_forward, input_reverse, max_mismatch_density=0.1):
    # type: (Path, int, int, Path, Path, float) -> Tuple[subprocess.Popen, str]
    output_prefix = input_forward.name.split('_', 1)[0] + '.fastq'
    command = [
        'flash',
        '--allow-outies',
        '-x', str(max_mismatch_density),
        '-o', str(output_prefix),
        '-m', str(min_overlap),
        '-M', str(max_overlap),
        '--compress',
        '-d', str(output_folder),
        str(input_forward), str(input_reverse)
    ]
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT), output_prefix


def run_vsearch_collapse(input_file, output_file):  # type: (Path, Path) -> subprocess.Popen
    command = [
        'vsearch',
        '--derep_fulllength',
        str(input_file),
        '--minuniquesize', str(2),
        '--output', '-',  # output fasta on stdout
        '--relabel', 'seq',
        '--sizeout',
        '--quiet'
    ]
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    with gzip.open(str(output_file), 'wt') as gz_out:
        for line in proc.stdout.readlines():
            gz_out.write(line)
    proc.wait()
    return proc


def run_vsearch_chimera(collapsed_input_file, chimera_output_file, non_chimera_output_file):
    # type: (Path, Path, Path) -> subprocess.Popen
    command = [
        'vsearch',
        '--abskew', str(4),
        '--alignwidth', str(0),
        '--mindiffs', str(1),
        '--uchime_denovo', str(collapsed_input_file),
        # "07_chimeras/" + primer + "/01_vsearchresults/" + indiv + ".out"
        '--uchimeout', str(chimera_output_file),
        # "07_chimeras/" + primer + "/02_preliminary/" + indiv + ".fasta"
        '--nonchimeras', str(non_chimera_output_file),
        '--quiet'
    ]
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def run_makeblastdb(input_file, output_file):  # type: (Path, Path) -> subprocess.Popen
    command = [
        'makeblastdb',
        '-in', str(input_file),
        '-parse_seqids',
        '-dbtype', 'nucl',
        '-out', str(output_file)  # rootfolder + "08_blast/" + primer + "/00_db/" + primer
    ]
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def run_blastn(db_path, non_chimeric_file, output_path):
    command = [
        'blastn',
        '-db', str(db_path),  # rootfolder + "08_blast/" + primer + "/00_db/" + primer
        '-query', str(non_chimeric_file),  # rootfolder + "07_chimeras/" + primer + "/03_nonchimeric/" + elem
        '-out', str(output_path),  # rootfolder + "08_blast/" + primer + "/01_blastresults/" + blastname
        '-max_target_seqs', str(1),
        '-outfmt', '10 qseqid evalue',
        '-num_threads', str(CPU_COUNT)
    ]
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def run_mafft(input_file, output_file):  # type: (Path, Path) -> subprocess.Popen
    command = [
        'mafft',
        '--auto',
        '--thread', str(CPU_COUNT),
        str(input_file),  # rootfolder + "09_align/" + primer + "/01_pooled/pooled.fasta
        # str(output_file)  # rootfolder + "09_align/" + primer + "/01_pooled/pooled_aligned.fasta"
    ]
    with open(str(output_file), 'wt') as outfile:
        proc = subprocess.Popen(command, stdout=outfile, stderr=sys.stderr)
        proc.wait()
        return proc
    # return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def run_entropy_analysis(input_file):  # type: (Path) -> subprocess.Popen
    command = [
        'entropy-analysis',
        '--quick',
        '--no-display',
        str(input_file),
    ]
    return subprocess.Popen(command, stdout=None, stderr=subprocess.PIPE)  # subprocess.PIPE


def run_oligotype(alignment_file, entropy_file, output_folder, selected_components, min_actual_abundance):
    # type: (Path, Path, Path, unicode, int) -> subprocess.Popen
    command = [
        'oligotype',
        str(alignment_file),  # rootfolder + "10_expand/" + primer + "/01_expanded/pooled.fasta"
        str(entropy_file),  # rootfolder + "11_entropy/" + primer + "/01_results/pooled.fasta-ENTROPY
        '-o', str(output_folder),  # rootfolder + "12_oligotype/" + primer + "/01_results/pooled
        '-C', str(selected_components),  # SELECTED_COMPONENTS
        '-s', str(1),  # MIN_NUMBER_OF_SAMPLES,
        '-a', str(3),  # MIN_PERCENT_ABUNDANCE,
        '-A', str(min_actual_abundance),
        '--skip-check-input-file',
        '--skip-basic-analyses',
        '--number-of-threads', str(CPU_COUNT),
    ]
    return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
