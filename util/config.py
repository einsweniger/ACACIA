# -*- coding: utf-8 -*-
from __future__ import division, print_function  # remove when py3

import ConfigParser
import argparse
import os
import sys
import textwrap

from typing import List, Tuple, TypeVar, Optional, Callable

from py3.py3pathlib import Path
from py3.py3shutil import which
from util.helpers import unfold_primer

T = TypeVar('T')


class InvalidUserInput(argparse.ArgumentTypeError):
    def __init__(self, error_msg):
        self.message = error_msg

    def __str__(self):
        return self.message


def ask_user(validation_fn, message):
    """
    gather and validate input from the user
    :param validation_fn: a callable that will receive the user input,
    should raise InvalidUserException when unsuccessful
    :param message: the message to present to the user, asking for their input
    :return: the return value of the validation_fn
    """
    while True:
        try:
            return validation_fn(raw_input(message))
        except EOFError:
            continue
        except InvalidUserInput as e_:
            print(e_)
            continue
        except KeyboardInterrupt:
            raise SystemExit(0)


def create_root_folder(path_str):  # type: (str) -> Path
    # (1) creates the basic folder system. Subfolders will be created later.
    if 0 == len(path_str):
        raise InvalidUserInput('input for work folder was empty')
    try:
        rootfolder = Path(path_str)
        rootfolder.mkdir(exist_ok=True, parents=True)
        return rootfolder
    except OSError as e_:
        raise InvalidUserInput(str(e_))


def validate_yes_no_question(user_input):  # type: (str) -> bool
    """
    validate user input for a yes or no question
    :param user_input: str
    :return: True if yes, False if no
    """
    msg = 'please answer with exactly one character (y/n)'
    if 1 != len(user_input):
        raise InvalidUserInput(msg)
    if user_input.upper() not in ('Y', 'N'):
        raise InvalidUserInput(msg)
    if 'Y' == user_input.upper():
        return True
    else:
        return False


def ignore_user_input(_):  # type: (str) -> None
    pass


def validate_primer_names(user_input):  # type: (str) -> List[str]
    if '' == user_input.strip():
        raise InvalidUserInput("please enter some primer names")
    if "." in user_input:
        raise InvalidUserInput("primer names may not contain dots (.)")
    return user_input.strip().split(", ")


def validate_int_or_zero(user_input):  # type: (str) -> int
    if "" == user_input.strip():
        return 0
    try:
        return int(user_input.strip())
    except ValueError:
        raise InvalidUserInput("please enter a number or nothing")


def validate_float(user_input):
    try:
        return float(user_input)
    except ValueError:
        raise InvalidUserInput(user_input + ' is not a valid float')


def validate_file_exists(user_input):  # type: (str) -> Path
    path = Path(user_input)
    if not path.is_file():
        raise InvalidUserInput("the file does not exist: " + user_input)
    return path


def validate_file_exists_or_empty(user_input):  # type: (str) -> Optional[Path]
    if '' == user_input.strip():
        return None
    return validate_file_exists(user_input)


def validate_sequence_expansion(user_input):  # type: (str) -> str
    try:
        unfold_primer(user_input)
    except KeyError:
        raise InvalidUserInput(user_input + ' is not a valid sequence')
    return user_input


class SubFolders(object):
    def __init__(self, root):  # type: (Path) -> None
        self.root = root
        self.archive = root / "01_archive"
        self.fastqcreports = root / "02_fastqcreports"
        self.trimquality = root / "03_trimquality"
        self.merge = root / "04_merge"
        self.qualityfilter = root / "05_qualityfilter"
        self.collapse = root / "06_collapse"
        self.chimeras = root / "07_chimeras"
        self.blast = root / "08_blast"
        self.align = root / "09_align"
        self.expand = root / "10_expand"
        self.entropy = root / "11_entropy"
        self.oligotype = root / "12_oligotype"
        self.reports = root / "13_reports"


class PrimerFolders(object):
    def __init__(self, subfolders, primer):  # type: (SubFolders, str) -> None
        self.folders = subfolders
        self.primer = primer

    @property
    def trimquality(self):
        class TrimSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.trimmed = parent / primer / '01_trimmed'
                self.fastqcreports = parent / primer / '02_fastqcreports'

        return TrimSubFolders(self.folders.trimquality, self.primer)

    @property
    def merge(self):
        class MergeSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.histograms = parent / primer / 'histograms'
                self.not_merged = parent / primer / 'not_merged'
                self.merged = parent / primer / 'merged'
                self.logs = parent / primer / 'logs'

        return MergeSubFolders(self.folders.merge, self.primer)

    @property
    def qualityfilter(self):
        class QualityFilterSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.primerfiltered = parent / primer / '01_primerfiltered'
                self.highquality = parent / primer / '02_highquality'

        return QualityFilterSubFolders(self.folders.qualityfilter, self.primer)

    @property
    def collapse(self):
        class CollapseSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.collapsed = parent / primer / 'collapsed'

        return CollapseSubFolders(self.folders.collapse, self.primer)

    @property
    def chimeras(self):
        class ChimerasSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.vsearchresults = parent / primer / '01_vsearchresults'
                self.preliminary = parent / primer / '02_preliminary'
                self.nonchimeric = parent / primer / '03_nonchimeric'

        return ChimerasSubFolders(self.folders.chimeras, self.primer)

    @property
    def blast(self):
        class BlastSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.db = parent / primer / '00_db'
                self.blastresults = parent / primer / '01_blastresults'
                self.blastclean = parent / primer / '02_blastclean'

        return BlastSubFolders(self.folders.blast, self.primer)

    @property
    def align(self):
        class AlignSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.pooled = parent / primer / '01_pooled'
                self.aligned = parent / primer / '02_aligned'

        return AlignSubFolders(self.folders.align, self.primer)

    @property
    def expand(self):
        class ExpandSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.expanded = parent / primer / '01_expanded'

        return ExpandSubFolders(self.folders.expand, self.primer)

    @property
    def entropy(self):
        class EntropySubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.results = parent / primer / '01_results'

        return EntropySubFolders(self.folders.entropy, self.primer)

    @property
    def oligotype(self):
        class OligotypeSubFolders(object):
            def __init__(self, parent, primer):  # type: (Path, str) -> None
                self.results = parent / primer / '01_results'

        return OligotypeSubFolders(self.folders.oligotype, self.primer)


def make_tuple_parser(constructor=str, size=None):
    # type: (Callable[[str],T], Optional[int]) -> Callable[[str],Tuple[T]]
    def make_tuple(user_input):  # type: (str) -> Tuple[T]
        splits = user_input.split(',')
        if None is not size and len(splits) != size:
            raise InvalidUserInput('input: ' + user_input + ' does not match expected size: ' + str(size))
        try:
            return tuple([constructor(x) for x in splits])
        except Exception as e_:
            raise InvalidUserInput(str(e_))

    return make_tuple


class AcaciaConfig(object):
    def __init__(self):
        self.name = 'ACACIA'
        self._get_config = self.try_get_setting
        # single parser run to get the root_folder, if any
        # pre_parser = argparse.ArgumentParser(description='ACACIA pipeline', add_help=False)
        # pre_parser.add_argument('folder', nargs='?', help='where to put your files')
        # pre_parser.add_argument('--primer-names', type=make_tuple_parser())
        # pre_pass_arguments, _ = pre_parser.parse_known_args()
        #
        # if None is not pre_pass_arguments.primer_names:
        #     primer_len = len(pre_pass_arguments.primer_names)
        #     if 0 == primer_len:
        #         primer_len = None
        # else:
        #     primer_len = None
        # parser = argparse.ArgumentParser(description='ACACIA pipeline')
        # parser.add_argument('folder', nargs='?', help='where to put your files')
        # parser.add_argument('--primer-names', type=make_tuple_parser(size=primer_len))
        # parser.add_argument('--skip-quality-reports', action='store_true')
        # trim_group = parser.add_argument_group(title='trimming', description='descr')
        # trim_group.add_argument('--trim-forward', type=make_tuple_parser(int, size=primer_len))
        # trim_group.add_argument('--trim-backward', type=make_tuple_parser(int, size=primer_len))
        # merge_group = parser.add_argument_group(title='merging', description='descr')
        # merge_group.add_argument('--skip-merge', action='store_true')
        # merge_group.add_argument('--merge-min-overlap', type=make_tuple_parser(int, size=primer_len))
        # merge_group.add_argument('--merge-max-overlap', type=make_tuple_parser(int, size=primer_len))
        # filter_group = parser.add_argument_group(title='filtering', description='descr')
        # filter_group.add_argument('--skip-filter', action='store_true')
        # filter_group.add_argument('--filter-forward',
        #                           type=make_tuple_parser(validate_sequence_expansion, size=primer_len))
        # filter_group.add_argument('--filter-reverse',
        #                           type=make_tuple_parser(validate_sequence_expansion, size=primer_len))
        # filter_group.add_argument('--filter-p', type=make_tuple_parser(int, size=primer_len))
        # filter_group.add_argument('--filter-q', type=make_tuple_parser(int, size=primer_len))
        # report_group = parser.add_argument_group(title='report', description='descr')
        # report_group.add_argument('--skip-collapsing', action='store_true')
        # report_group.add_argument('--blast-reference', type=validate_file_exists)
        # report_group.add_argument('--known-alleles', type=make_tuple_parser(size=primer_len))
        # report_group.add_argument('--alleles-locus', type=make_tuple_parser(size=primer_len))
        # # todo, expected input is primer names where we want to remove singletons
        # report_group.add_argument('--remove-singletons',
        #                           type=make_tuple_parser(validate_yes_no_question, size=primer_len))
        # report_group.add_argument('--abs-nor', type=make_tuple_parser(float, size=primer_len))
        # report_group.add_argument('--low-por', type=make_tuple_parser(float, size=primer_len))

        # use args from pre_pass to determine if we have a config file
        # if None is not pre_pass_arguments.folder:
        #     data_folder = Path(pre_pass_arguments.folder)
        #     self.root_folder = data_folder
        #     config_path = data_folder / 'config.ini'
        #     if config_path.is_file():
        #         defaults = self.parse_cfg()
        #         parser.set_defaults(**defaults)
        #         # add config to parser defaults
        #         self.folders = SubFolders(self.root_folder)
        #         pass
        #
        # arguments = parser.parse_args()
        #
        # if arguments.folder is not None:
        #     self.root_folder = Path(arguments.folder)  # type: Path
        #     self.folders = SubFolders(self.root_folder)
        #     self.merge_input_folder = self.folders.archive  # type: Path
        #
        # else:
        #     self.root_folder = None
        #     self.folders = None
        #     self.sub_folders = None
        #     self.merge_input_folder = None
        self.root_folder = None  # type: Optional[Path]
        self.folders = None  # type: Optional[SubFolders]
        if len(sys.argv) > 1:
            root = Path(sys.argv[1])
            if root.is_dir():
                self.root_folder = root
                self.folders = SubFolders(root)
                self.merge_input_folder = self.folders.archive
        self.merge_input_folder = None  # type: Optional[Path]
        self.config_parser = ConfigParser.RawConfigParser()
        self.primers = []  # type: List[PrimerConfig]

    def write_cfg(self):
        cfg_path = self.root_folder / 'config.ini'
        with open(str(cfg_path), 'wt') as cfg_file:
            self.config_parser.write(cfg_file)

    def try_get_setting(self, section, key, validation=None, kind=None, default=None):
        getters = {
            bool:  self.config_parser.getboolean,
            None:  self.config_parser.get,
            str:   self.config_parser.get,
            int:   self.config_parser.getint,
            float: self.config_parser.getfloat,
        }
        getter = getters[kind]
        try:
            value = getter(section, key)
            if None is validation:
                return value
            return validation(value)
        except ConfigParser.NoOptionError:
            return default

    def _set_config(self, key, value):
        if not self.config_parser.has_section(self.name):
            self.config_parser.add_section(self.name)
        self.config_parser.set(self.name, key, value)
        self.write_cfg()
        # welp, configparser tries to call bool.lower() which fails if it is not read back :/
        self.parse_cfg()

    @property
    def blast_reference_file(self):  # type: (AcaciaConfig) -> Optional[Path]
        return self._get_config(self.name, 'blast_reference_file', validation=validate_file_exists)

    @blast_reference_file.setter
    def blast_reference_file(self, value):  # type: (AcaciaConfig, Path) -> None
        self._set_config('blast_reference_file', value)

    @property
    def gen_quality_reports(self):  # type: (AcaciaConfig) -> Optional[bool]
        return self._get_config(self.name, 'gen_quality_reports', kind=bool)

    @gen_quality_reports.setter
    def gen_quality_reports(self, value):  # type: (AcaciaConfig, bool) -> None
        self._set_config('gen_quality_reports', value)

    @property
    def gen_trimmed_quality_reports(self):  # type: (AcaciaConfig) -> Optional[bool]
        return self._get_config(self.name, 'gen_trimmed_quality_reports', kind=bool)

    @gen_trimmed_quality_reports.setter
    def gen_trimmed_quality_reports(self, value):  # type: (AcaciaConfig, bool) -> None
        self._set_config('gen_trimmed_quality_reports', value)

    @property
    def collapse_sequences(self):  # type: (AcaciaConfig) -> Optional[bool]
        return self._get_config(self.name, 'collapse_sequences', kind=bool)

    @collapse_sequences.setter
    def collapse_sequences(self, value):  # type: (AcaciaConfig, bool) -> None
        self._set_config('collapse_sequences', value)

    @property
    def merge_reads(self):  # type: (AcaciaConfig) -> Optional[bool]
        return self._get_config(self.name, 'merge_reads', kind=bool)

    @merge_reads.setter
    def merge_reads(self, value):  # type: (AcaciaConfig, bool) -> None
        self._set_config('merge_reads', value)

    @property
    def filter_sequences(self):  # type: (AcaciaConfig) -> Optional[bool]
        return self._get_config(self.name, 'filter_sequences', kind=bool)

    @filter_sequences.setter
    def filter_sequences(self, value):  # type: (AcaciaConfig, bool) -> None
        self._set_config('filter_sequences', value)

    def parse_cfg(self):
        if None is self.root_folder:
            return
        cfg_path = self.root_folder / 'config.ini'
        if not cfg_path.is_file():
            return

        self.config_parser.read(str(cfg_path))
        self.primers = []
        for section in self.config_parser.sections():
            if self.name == section:
                continue
            self.primers.append(PrimerConfig(section, self))

    def check(self):
        executables_to_check = ['fastqc', 'vsearch', 'mafft', 'entropy-analysis', 'oligotype', 'blastn', 'makeblastdb']
        missing_executables = []
        for ex in executables_to_check:
            if None is which(ex):
                missing_executables.append(ex)
        if 0 < len(missing_executables):
            print('missing executables:', *missing_executables)
            raise SystemExit(1)

        if None is self.root_folder:
            self.root_folder = ask_user(create_root_folder, textwrap.dedent("""
            Please enter the full path of the working directory for this analysis (e.g. /home/user/myproject/).
            New folders and files will be written into that directory:
            """))

        self.root_folder.mkdir(exist_ok=True)
        self.parse_cfg()
        if not self.config_parser.has_section(self.name):
            self.config_parser.add_section(self.name)
        self.folders = SubFolders(self.root_folder)
        self.merge_input_folder = self.folders.archive
        os.chdir(str(self.root_folder))

        """
        Read primer pairs from user, here: "ch1, ch2"
        for each primer:
            create dir, then
            Prompt user to extract/place files there, wait for RETURN
        """
        if 0 == len(self.primers):
            primer_names = ask_user(validate_primer_names, textwrap.dedent("""

            #1: Please enter the name of each primer pair used in the run that will be analyzed here.
            If you used more than one pair, separate their names with a comma and a space
            (f.ex. Primerpair1, Primerpair2, Primerpair3).
            Please never use points (.) in the names:
            """))
            for name in primer_names:
                self.primers.append(PrimerConfig(name, self))
        for primer in self.primers:  # creates a subfolder for each primer pair in archive
            primer_path = self.folders.archive / primer.name
            if not primer_path.exists():
                primer_path.mkdir(parents=True)
                ask_user(ignore_user_input, textwrap.dedent("""
                Please copy the raw fastq.gz files generated with the primer pair {primer}
                into the folder {primer_path}.
                Press any key when you are done...
                """).format(primer=primer, primer_path=primer_path))

        # step 2
        if None is self.gen_quality_reports:
            self.gen_quality_reports = ask_user(validate_yes_no_question, textwrap.dedent("""
            # 2: In the following step, quality reports can be generated for each fastq file,
            which can help you deciding to trim bases from your reads.
            Would you like ACACIA to produce quality reports of your fastq files
            (Y/N)?  
            """))

    def check_trim(self):
        # step 3
        trim_count = 0
        for primer in self.primers:
            if None is primer.trim_r1:
                continue
            if None is primer.trim_r2:
                continue
        else:
            preamble = textwrap.dedent("""
            Based on the results of the quality reports (the previous step), you can now trim any number of bases from 
            the END (right tip) of all R1 and R2 files (those with the forward/reverse reads) of the primers. 
            Please enter the number here, or just press ENTER if nothing should be trimmed.
            """)
            print(preamble)
        for primer in self.primers:
            if None is primer.trim_r1:
                primer.trim_r1 = ask_user(
                    validate_int_or_zero,
                    'Trim amount for R1 files of primer {primer}: '.format(primer=primer.name))
                trim_count += primer.trim_r1
            if None is primer.trim_r2:
                primer.trim_r2 = ask_user(
                    validate_int_or_zero,
                    'Trim amount for R2 files of primer {primer}: '.format(primer=primer.name))
                trim_count += primer.trim_r2
        if 0 < trim_count and None is self.gen_trimmed_quality_reports:
            self.merge_input_folder = self.folders.trimquality
            self.gen_trimmed_quality_reports = ask_user(validate_yes_no_question, textwrap.dedent("""
            You chose to trim raw reads.
            Would you like to generate fastqc reports for the new, trimmed fastq files?
            (Y/N):
            """))

        if None is which('fastqc'):
            print('fastqc executable not found, skipping quality report generation')
            self.gen_quality_reports = False  # step 2
            self.gen_trimmed_quality_reports = False  # step 3

    def check_merge(self):
        # step 4
        # todo: step 4.2 input folder, use trimmed data to merge or raw data?
        # todo: if trimmed data exists, favor that over raw data?
        if None is self.merge_reads:
            self.merge_reads = ask_user(validate_yes_no_question, textwrap.dedent("""
            #4: The following step will merge your forward & reverse reads into one.
            Enter "Y" if you would like ACACIA to do it for you, 
            or "N" if you did it already and want to skip this step
            (Y/N):
            """))
        if self.merge_reads:
            for primer in self.primers:
                # todo: check if min overlap <= max overlap?
                if None is primer.merge_min_overlap:
                    primer.merge_min_overlap = ask_user(
                        validate_int_or_zero,
                        textwrap.dedent("""
                        Enter the MINIMAL number of bases expected to overlap when merging
                        forward and reverse reads of the primer pair {primer}:
                        """.format(primer=primer.name)))
                if None is primer.merge_max_overlap:
                    primer.merge_max_overlap = ask_user(
                        validate_int_or_zero,
                        textwrap.dedent("""
                        Enter the MAXIMAL number of bases expected to overlap when merging
                        forward and reverse reads of the primer pair {primer}:
                        """.format(primer=primer.name)))
        if which('vsearch') is None:
            print('cannot run filter step without the vsearch executable')
            self.filter_sequences = False
        if None is self.filter_sequences:
            self.filter_sequences = ask_user(
                validate_yes_no_question,
                textwrap.dedent("""
                #5: This will now discard all reads missing perfect primers.
                Then, it will trim off all primers.
                Finally, it will filter sequences based on quality.
                perform this step? (y/n)
                """))

    def check_filter(self):
        if not self.filter_sequences:
            self.collapse_sequences = False
        else:
            self.collapse_sequences = True
        if self.filter_sequences:
            for primer in self.primers:
                if None is primer.filter_forward:
                    primer.filter_forward = ask_user(
                        validate_sequence_expansion,
                        textwrap.dedent("""
                        Please enter the sequence of the FORWARD primer in the 5' > 3' direction.
                        for primer {primer}:
                        """.format(primer=primer.name)))
                if None is primer.filter_reverse:
                    primer.filter_reverse = ask_user(
                        validate_sequence_expansion,
                        textwrap.dedent("""
                        Please enter the sequence of the REVERSE primer in the 5' > 3' direction.
                        for primer {primer}:
                        """.format(primer=primer.name)))

        if None is self.blast_reference_file:
            text = textwrap.dedent("""
            A BLAST database needs to be created for the BLAST filter.
            Please enter here the full path to a fasta file that should be used as reference
              (f.ex. /home/user/somefolder/relatedsequences.fasta).
            A BLAST database will be generated and your sequences will be BLASTed against it
            db_file: """)
            self.blast_reference_file = ask_user(validate_file_exists, text)

        ask_filter_qp = False
        for primer in self.primers:
            if primer.filter_p is None:
                ask_filter_qp = True
                break
            if primer.filter_q is None:
                ask_filter_qp = True
                break
        if ask_filter_qp:
            print(textwrap.dedent("""
            This will start a filter based on quality. 
            The parameters that can be changed are: 
            -q (minimal Phred-score for quality filter)  and 
            -p (minimal percentage of the bases within a read that need to reach -q, in order for the read to be kept)
            """))
            for primer in self.primers:
                if None is primer.filter_q:
                    primer.filter_q = ask_user(
                        validate_int_or_zero,
                        '-q for primer {primer}: '.format(primer=primer.name))
                if None is primer.filter_p:
                    primer.filter_p = ask_user(
                        validate_int_or_zero,
                        '-p for primer {primer}: '.format(primer=primer.name))

    def check_alleles(self):
        for primer in self.primers:
            if None is primer.known_alleles:
                text = textwrap.dedent("""
                Considering primer {primer}:
                If there are previously known alleles whose names should be considered here,
                please generate a simple fasta file with their names and sequences.
                Enter the full path to the fasta file here (f.ex. /home/user/project/allelenames.fasta).
                Otherwise just press ENTER, and all alleles will be named from scratch:
                """)
                primer.known_alleles = ask_user(validate_file_exists_or_empty, text.format(primer=primer.name))
        for primer in self.primers:
            if None is primer.alleles_locus:
                text = textwrap.dedent("""
                Please enter the symbol of the locus being genotyped with the primer pair {primer}
                (f.ex. Prlo-DRB, for the DRB gene of Procyon lotor).
                That symbol will be used for naming new alleles:
                """.format(primer=primer.name))
                primer.alleles_locus = raw_input(text)
                # todo: 'test-locus' is a valid input, are there others? Do we want validation here?
        for primer in self.primers:
            if None is primer.remove_singletons:
                text = textwrap.dedent("""
                Should singletons (alleles yielded in one single file) be removed
                for primer {primer}? (y/n): 
                """.format(primer=primer.name))
                primer.remove_singletons = ask_user(validate_yes_no_question, text)
        ask_abs_low = False
        for primer in self.primers:
            if None is primer.low_por:
                ask_abs_low = True
                break
            if None is primer.abs_nor:
                ask_abs_low = True
                break
        if ask_abs_low:
            print(textwrap.dedent("""
            Working now on each individual.
            You will now need to set two thresholds:
            A) "abs_nor": "absolute smallest number of reads" that an allele has to reach in one ID, 
              in order to be called in that ID. Independent of read depth.
              Range = Between 0 and 1000
              Default = 10.
            B) "low_por": "lowest proportion of reads" supporting an allele in an amplicon, 
              in order for that allele to be called in that ID.
              Range = Between 0 and 1.
              Default = 0, but 0.01 is recommended for large data sets (over 50,000 x read depth or over 200 IDs), 
              which can suffer more from false positives.
            """))
            for primer in self.primers:
                if None is primer.abs_nor:
                    primer.abs_nor = ask_user(validate_float, 'primer {primer} abs_nor: '.format(primer=primer.name))
                if None is primer.low_por:
                    primer.low_por = ask_user(validate_float, 'primer {primer} low_por: '.format(primer=primer.name))


class PrimerConfig(object):
    def __init__(self, name, acacia_conf):  # type: (str, AcaciaConfig) -> None
        self.name = name
        self._cfg_parser = acacia_conf.config_parser
        self._get_config = acacia_conf.try_get_setting
        self._ac_config = acacia_conf
        self.folders = PrimerFolders(acacia_conf.folders, self.name)
        if not self._cfg_parser.has_section(self.name):
            self._cfg_parser.add_section(self.name)
            self._ac_config.write_cfg()

    def _set_config(self, key, value):
        if not self._cfg_parser.has_section(self.name):
            self._cfg_parser.add_section(self.name)
        self._cfg_parser.set(self.name, key, value)
        self._ac_config.write_cfg()

    def __dict__(self):
        return {
            'trim_front': self.trim_r1,
            'trim_end': self.trim_r2,
            'merge_min_overlap': self.merge_min_overlap,
            'merge_max_overlap': self.merge_max_overlap,
            'filter_forward': self.filter_forward,
            'filter_reverse': self.filter_reverse,
            'filter_q': self.filter_q,
            'filter_p': self.filter_p,
            'known_alleles': self.known_alleles,
            'alleles_locus': self.alleles_locus,
            'remove_singletons': self.remove_singletons,
            'abs_nor': self.abs_nor,
            'low_por': self.low_por,
        }

    @property
    def trim_r1(self):  # type: (PrimerConfig) -> Optional[int]
        return self._get_config(self.name, 'trim_front', kind=int)

    @trim_r1.setter
    def trim_r1(self, value):  # type: (PrimerConfig, int) -> None
        self._set_config('trim_front', value)

    @property
    def trim_r2(self):  # type: (PrimerConfig) -> Optional[int]
        return self._get_config(self.name, 'trim_end', kind=int)

    @trim_r2.setter
    def trim_r2(self, value):  # type: (PrimerConfig, int) -> None
        self._set_config('trim_end', value)
        
    @property
    def merge_min_overlap(self):  # type: (PrimerConfig) -> Optional[int]
        return self._get_config(self.name, 'merge_min_overlap', kind=int)
    
    @merge_min_overlap.setter
    def merge_min_overlap(self, value):  # type: (PrimerConfig, int) -> None
        self._set_config('merge_min_overlap', value)
            
    @property
    def merge_max_overlap(self):  # type: (PrimerConfig) -> Optional[int]
        return self._get_config(self.name, 'merge_max_overlap', kind=int)
    
    @merge_max_overlap.setter
    def merge_max_overlap(self, value):  # type: (PrimerConfig, int) -> None
        self._set_config('merge_max_overlap', value)    

    @property
    def filter_forward(self):  # type: (PrimerConfig) -> Optional[str]
        return self._get_config(self.name, 'filter_forward', kind=str)

    @filter_forward.setter
    def filter_forward(self, value):  # type: (PrimerConfig, str) -> None
        self._set_config('filter_forward', value)

    @property
    def filter_reverse(self):  # type: (PrimerConfig) -> Optional[str]
        return self._get_config(self.name, 'filter_reverse', kind=str)

    @filter_reverse.setter
    def filter_reverse(self, value):  # type: (PrimerConfig, str) -> None
        self._set_config('filter_reverse', value)
    
    @property
    def filter_p(self):  # type: (PrimerConfig) -> Optional[int]
        return self._get_config(self.name, 'filter_p', kind=int)
    
    @filter_p.setter
    def filter_p(self, value):  # type: (PrimerConfig, int) -> None
        self._set_config('filter_p', value)
            
    @property
    def filter_q(self):  # type: (PrimerConfig) -> Optional[int]
        return self._get_config(self.name, 'filter_q', kind=int)
    
    @filter_q.setter
    def filter_q(self, value):  # type: (PrimerConfig, int) -> None
        self._set_config('filter_q', value)
            
    @property
    def known_alleles(self):  # type: (PrimerConfig) -> Optional[str]
        return self._get_config(self.name, 'known_alleles', kind=str)
    
    @known_alleles.setter
    def known_alleles(self, value):  # type: (PrimerConfig, str) -> None
        self._set_config('known_alleles', value)
            
    @property
    def alleles_locus(self):  # type: (PrimerConfig) -> Optional[str]
        return self._get_config(self.name, 'alleles_locus', kind=str)
    
    @alleles_locus.setter
    def alleles_locus(self, value):  # type: (PrimerConfig, str) -> None
        self._set_config('alleles_locus', value)
            
    @property
    def remove_singletons(self):  # type: (PrimerConfig) -> Optional[bool]
        return self._get_config(self.name, 'remove_singletons', kind=bool)
    
    @remove_singletons.setter
    def remove_singletons(self, value):  # type: (PrimerConfig, bool) -> None
        self._set_config('remove_singletons', value)
            
    @property
    def abs_nor(self):  # type: (PrimerConfig) -> Optional[float]
        return self._get_config(self.name, 'abs_nor', kind=float)
    
    @abs_nor.setter
    def abs_nor(self, value):  # type: (PrimerConfig, float) -> None
        self._set_config('abs_nor', value)
            
    @property
    def low_por(self):  # type: (PrimerConfig) -> Optional[float]
        return self._get_config(self.name, 'low_por', kind=float)
    
    @low_por.setter
    def low_por(self, value):  # type: (PrimerConfig, float) -> None
        self._set_config('low_por', value)
