#!/home/santos/anaconda2/bin/python
# -*- coding: utf-8 -*-
from __future__ import division, print_function  # remove when py3

import textwrap

import numpy as np
import pandas as pd
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from typing import Tuple, Optional

from py3.py3pathlib import Path
from util import commands
from util.config import AcaciaConfig
from util.helpers import *

ICON = """\

       ///
      *///////
       //////////     ///
        //////////    //         *////
          ////////// ./     ///////////
            //////// //   ////////////
              .////  // .////////////
                     // ///////////
                     ///////////
                    ///
                    //
                   ///
                   ///
                  ,///
                  ////
                  ////
                  ///.
                  ///
"""

""" TODO:
Python 3 migration:
  - replace all raw_input() through input() when py3

Figure this out for error handling:
  - some steps depend on other steps, which ones do?
    maybe solve this by _not_ creating the output folders as a first step
    and use missing folders as a safe-guard?
    Or create a dependency chain between the pipeline steps.
"""


class Bcolors(object):
    """usage: print(bcolors.YELLOW + "This is a warning" + bcolors.END"""
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    END = "\033[0m"


def yellow_text(string):
    return Bcolors.YELLOW + string + Bcolors.END


def blue_text(string):
    return Bcolors.BLUE + string + Bcolors.END


def green_text(string):
    return Bcolors.GREEN + string + Bcolors.END


def red_text(string):
    return Bcolors.RED + string + Bcolors.END


def show_banner():
    print(yellow_text("This is"),
          blue_text("ACACIA") + yellow_text(": Allele Calling proCedure for Illumina Amplicon sequencing."))
    print(blue_text(ICON))
    print(yellow_text(textwrap.dedent("""\
    It will call and name alleles out of your Illumnia raw data quickly and precisely.
    This pipeline requires currently 8 pieces of software that need to be installed in the machine running the workflow:

    1. Python 2.7 for keeping it all together
    2. FASTQC for initial quality reports (https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
    3. FLASh for merging reads (https://ccb.jhu.edu/software/FLASH/)
    4. VSEARCH for merging reads, dereplication and chimera detection (https://github.com/torognes/vsearch)
    5. BLAST for BLASTing (ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST)
    6. MAFFT for aligning (https://mafft.cbrc.jp/alignment/software)
    7. OLIGOTYPING for clustering and pre-calling of alleles (http://merenlab.org/software/oligotyping)
    8. The Pandas library for handling tables (https://pandas.pydata.org)

    You will also need:
    1. The paired-end raw data files
    2. The names and sequences of your primers
    3. A fasta file with related sequences to be set up as a local BLAST database

    Please get everything together and all software up & running before you go on with the pipeline. Good luck!
    """)))
    try:
        _ = raw_input(yellow_text("Hit ENTER now if you are ready to go. " +
                                  "Otherwise press Ctrl + D to exit and restart later."))
    except EOFError:
        print("see you soon!")
        exit(0)


def delete_folder_content(folder):  # type: (Path) -> None
    if not folder.exists() or not folder.is_dir():
        return
    for item in folder.iterdir():
        if item.is_dir():
            delete_folder_content(item)
            item.rmdir()
            continue
        item.unlink()
        # add more where necessary


def main():
    # show_banner()
    config = AcaciaConfig()


    ##############################################################################################################
    # creates and begins to populate the monitoring dataframe
    config.check()

    # monitoring_df is used throughout step 4-10
    monitoring_df = build_monitoring_df(config)
    ##############################################################################################################
    # (2) Produces fastqc quality reports for each fastq file.
    make_quality_reports(config)

    ###############################################################################################################
    # (3) Offers to trim out the ends of forward (R1) and reverse reads (R2), based on the results of fastqc (2).
    # Produces fastqc quality reports for each new fastq file.
    config.check_trim()
    trim_primers(config)
    # build the dictionaries
    config.check_merge()
    merge_with_flash(config, monitoring_df)
    config.check_filter()
    discard_trim_and_filter(config, monitoring_df)
    collapse_sequences(config, monitoring_df)
    filter_chimeras(config, monitoring_df)
    blast_sequences(config, monitoring_df)
    align_fasta(config)
    convert_for_oligotype(config)
    analyze_entropy(config)
    oligotype_analysis(config, monitoring_df)
    config.check_alleles()
    extract_alleles(config, monitoring_df)


def maybe_run_fastqc(input_folder, output_folder):  # type: (Path, Path) -> Tuple[List[Path], List[Path], List[Path]]
    def need_to_run_fastqc(primer_path):  # type: (Path) -> bool
        for suffix in ('.html', '.zip'):
            report_file = output_folder / primer_path.name.replace('.fastq.gz', '_fastqc' + suffix)
            if not report_file.exists():
                return True
        return False

    skipped = []  # type: List[Path]
    successful = []  # type: List[Path]
    failed = []  # type: List[Path]

    output_folder.mkdir(parents=True, exist_ok=True)
    input_files = sorted_natural(input_folder.iterdir())
    for i, input_file in enumerate(input_files):  # run fastqc
        # print(status_message.format(percentage=status, primer=input_file.name))
        print_progress(i+1, len(input_files), 'quality reports', input_file.name)
        if not need_to_run_fastqc(input_file):
            skipped.append(input_file)
            continue
        proc = commands.run_fastqc(input_file, output_folder)
        out = [line.decode('utf-8') for line in proc.stdout.readlines()]
        err = [line.decode('utf-8') for line in proc.stderr.readlines()]
        # note: we can only depend on text output from both stderr and stdout
        # both are not used in a consistent manner :/
        # return codes are also 0 for some manners of failed processing.
        for output in (out, err):
            for line in output:
                if line.startswith("Analysis complete"):
                    successful.append(input_file)
                    break
                elif line.startswith("Failed to process"):
                    print(red_text('\n'.join(output)))
                    failed.append(input_file)
                    break

    return skipped, successful, failed


def build_monitoring_df(config):  # type: (AcaciaConfig) -> pd.DataFrame
    mon_file = config.folders.root / 'mon.csv'

    def build_index_tuples():
        for pr in config.primers:
            for pa in (config.root_folder / pr.name).glob('*_R1_*'):
                yield pr, pa.name.split("_")[0]

    index_tuples = list(build_index_tuples())
    multi_index = pd.MultiIndex.from_tuples(index_tuples, names=['PRIMER', 'ID'])

    if mon_file.is_file():
        monitoring_df = pd.read_csv(str(mon_file)).set_index(['PRIMER', 'ID'])
    else:
        monitoring_df = pd.DataFrame(index=multi_index)
    if 'RAW_READS' in monitoring_df:
        raw_series = monitoring_df.RAW_READS
    else:
        raw_series = pd.Series(index=multi_index)
    for primer in config.primers:
        def check_if_r2_exists(r1_path):
            r2_path = r1_path.parent / r1_path.name.replace('_R1_', '_R2_')
            return r2_path.is_file()

        input_folder = config.folders.archive / primer.name
        r1_files = list(sorted_natural(input_folder.glob('*_R1_*.fastq.gz')))
        assert all([check_if_r2_exists(r1) for r1 in r1_files])
        print("\nThere are", len(r1_files), "pairs of files for", primer.name)
        for index, path in enumerate(r1_files):
            print_progress(index + 1, len(r1_files), 'processing ' + primer.name, path.name)
            individual = path.name.split("_")[0]
            if (primer.name, individual) in raw_series:
                if raw_series[(primer.name, individual)] != 0:
                    continue
            with gzip.open(str(path), "rt") as infastq:
                k = len(list(SeqIO.parse(infastq, "fastq")))
                raw_series[(primer.name, individual)] = k
                # monitoring_df = monitoring_df.append({
                #     "PRIMER": primer,
                #     "ID": individual,
                #     "RAW_READS": k}, ignore_index=True)
                # print("Processed file", k, path.name)
    monitoring_df['RAW_READS'] = raw_series
    monitoring_df.to_csv(str(config.root_folder / 'mon.csv'))
    return monitoring_df


def make_quality_reports(config):  # type: (AcaciaConfig) -> None
    """STEP 2
    Produces fastqc quality reports for each fastq file.
    """
    if not config.gen_quality_reports:
        print("Step 2: skipped")
        return
    print("Step 2: Producing quality reports...")
    for primer in config.primers:
        output_folder = config.folders.fastqcreports / primer.name
        output_folder.mkdir(parents=True, exist_ok=True)
        primers_dir = config.folders.archive / primer.name
        maybe_run_fastqc(input_folder=primers_dir, output_folder=output_folder)


def trim_primers(config):  # type: (AcaciaConfig) -> None
    """STEP 3
    Offers to trim out the ends of forward (R1) and reverse reads (R2),
    based on the results of fastqc (2).
    Produces fastqc quality reports for each new fastq file.
    """
    trim_count = 0
    for primer in config.primers:
        trim_count += primer.trim_r1
        trim_count += primer.trim_r2
    if 0 == trim_count:
        # nothing to do.
        print("Step 3: skipped")
        return
    print("Step 3: trim raw reads")
    for primer in config.primers:
        sub_folders = primer.folders.trimquality
        # trimmer
        input_folder = config.folders.archive / primer.name

        def trim(in_file, out_dir, trim_size):  # type: (Path, Path, int) -> None
            if (out_dir / in_file.name).exists():
                print('trimmed file exists, skipping {input}'.format(input=in_file))
                return
            with gzip.open(str(in_file), 'rt') as fastq_in, gzip.open(str(out_dir / in_file.name), 'wt') as fastq_out:
                for seq_record in SeqIO.parse(fastq_in, 'fastq'):
                    SeqIO.write(seq_record[:-trim_size], fastq_out, 'fastq')

        if 0 < primer.trim_r1:
            input_files = sorted_natural(input_folder.glob('*_R1_*'))
            sub_folders.trimmed.mkdir(exist_ok=True, parents=True)

            for i, input_file in enumerate(input_files):
                print_progress(i+1, len(input_files), 'trim r1', primer.name)
                trim(input_file, sub_folders.trimmed, primer.trim_r1)
        else:
            print("\nNothing to trim from the forward reads of " + primer.name)
        if 0 < primer.trim_r2:
            input_files = sorted_natural(input_folder.glob('*_R2_*'))
            sub_folders.trimmed.mkdir(exist_ok=True, parents=True)
            for i, input_file in enumerate(input_files):
                print_progress(i + 1, len(input_files), 'trim r2', primer.name)
                trim(input_file, sub_folders.trimmed, primer.trim_r2)
        else:
            print("Nothing to trim from the reverse reads of " + primer.name)

        # in case trimming was performed, produce fastqc quality reports for each new fastq file.
        if not config.gen_trimmed_quality_reports:
            continue
        sub_folders.fastqcreports.mkdir(exist_ok=True, parents=True)
        maybe_run_fastqc(input_folder, sub_folders.fastqcreports)


def merge_with_flash(config, monitoring_df):  # type: (AcaciaConfig, pd.DataFrame) -> None
    ##############################################################################################################
    # (4) Uses flash to merge reads
    # (4.1) first determine the degree of overlap for each primer pair
    if not config.merge_reads:
        print('skipping step 4')
        return
    print('running step 4')
    # (4.2) runs FLASh
    if 'MERGED' in monitoring_df:
        merged_series = monitoring_df['MERGED']
    else:
        merged_series = pd.Series(index=monitoring_df.index, dtype='uint')
    for primer in config.primers:
        sub_folders = primer.folders.merge
        for folder_name in ("histograms", "logs", "merged", "not_merged"):
            root_output_folder = config.folders.merge / primer.name / folder_name
            root_output_folder.mkdir(exist_ok=True, parents=True)

        primer_folder = config.merge_input_folder / primer.name
        forward_inputs = sorted_natural(primer_folder.glob('*_R1_*'))  # type: List[Path]

        for index, forward_input in enumerate(forward_inputs):
            print_progress(index+1, len(forward_inputs), 'merge ' + primer.name, forward_input.name)
            reverse_input = forward_input.parent / forward_input.name.replace('_R1_', '_R2_')
            tmp_folder = config.folders.merge / primer.name / 'tmp'
            if tmp_folder.is_dir():
                delete_folder_content(tmp_folder)
                tmp_folder.rmdir()
            output_prefix = forward_input.name.split('_', 1)[0] + '.fastq'
            log_file = sub_folders.logs / (output_prefix + '.log')
            if log_file.is_file():
                continue
            p, prefix = commands.run_flash(
                tmp_folder,
                primer.merge_min_overlap,
                primer.merge_max_overlap,
                forward_input,
                reverse_input
            )
            proc_output = [line.decode('utf-8') for line in p.stdout.readlines()]
            for output_file in tmp_folder.iterdir():
                name = output_file.name
                if '.hist' in output_file.name:
                    output_file.rename(sub_folders.histograms / name)
                elif '.notCombined' in output_file.name:
                    output_file.rename(sub_folders.not_merged / name)
                elif '.extendedFrags' in output_file.name:
                    output_file.rename(sub_folders.merged / name)
                else:
                    output_file.unlink()
                    print(yellow_text('removed remaining file ' + str(output_file)))
            with open(str(log_file), 'wt') as out_file:
                out_file.write(str('\n'.join(proc_output)))
            tmp_folder.rmdir()

        print(Bcolors.GREEN + "\nSaving results now." + Bcolors.END)
        for index, elem in enumerate(sub_folders.merged.iterdir()):
            print_progress(index + 1, len(forward_inputs), 'merge ' + primer.name, elem.name)
            assert ".fastq.extendedFrags." in elem.name
            indiv = elem.name.split(".fastq.extendedFrags.")
            indiv = indiv[0]
            if (primer.name, indiv) in merged_series:
                if 0 != merged_series[(primer.name, indiv)]:
                    continue
            merged_series[(primer.name, indiv)] = count_records(elem, 'fastq')
    monitoring_df['MERGED'] = merged_series
    monitoring_df.to_csv(str(config.folders.root / 'mon.csv'))


def discard_trim_and_filter(config, monitoring_df):  # type: (AcaciaConfig, pd.DataFrame) -> None
    """
    (5) First discards all reads missing primers, then trimms off the primers.
    Finally performs quality filter.
    :param config:
    :param monitoring_df:
    :return:
    """
    if not config.filter_sequences:
        print('skipping discard->trim->filter step (5)')
        return
    print('running discard->trim->filter step (5)')
    if 'PRIMER_FILTERED' not in monitoring_df:
        filtered_series = pd.Series(index=monitoring_df.index, dtype='uint')
    else:
        filtered_series = monitoring_df['PRIMER_FILTERED']
    if 'QC_FILTERED' not in monitoring_df:
        qc_filtered_series = pd.Series(index=monitoring_df.index, dtype='uint')
    else:
        qc_filtered_series = monitoring_df['PRIMER_FILTERED']
    for primer in config.primers:
        sub_folders = primer.folders.qualityfilter
        sub_folders.primerfiltered.mkdir(parents=True, exist_ok=True)
        sub_folders.highquality.mkdir(parents=True, exist_ok=True)
        # write the fastq files into /05_qualityfilter/[primer]/01_primerfiltered/:
        # for primer in config.primer_names:
        merged_primer_folder = primer.folders.merge.merged
        merged_files = sorted_natural(merged_primer_folder.iterdir())
        # process_files = []
        # for merged_input_path in merged_files:
        #     output_name = merged_input_path.name.replace(".fastq.extendedFrags.fastq.gz", ".fastq.gz")
        #     output_path = output_folder / output_name
        #     if not output_path.is_file():
        #         process_files.append((merged_input_path, output_path))

        for index, merged_input_path in enumerate(merged_files):
            output_name = merged_input_path.name.replace(".fastq.extendedFrags.fastq.gz", ".fastq.gz")
            output_path = sub_folders.primerfiltered / output_name
            print_progress(index+1, len(merged_files), 'filtering ' + primer.name, output_name)
            if output_path.is_file():
                # print('skipped', str(output_path))
                continue

            def filter_primer(forward_sequence, reverse_sequence, current_record):
                # type: (str, str, SeqRecord) -> Optional[SeqRecord]
                current_sequence = current_record.seq  # type: Seq
                start = current_sequence.find(forward_sequence)
                if -1 == start:
                    return None
                # todo: if this replaces the loop code, try to use find with start parameter.
                # this will speed up the search:
                # 1. no comparison across whole seq necessary
                # 2. no need to check if start < end
                # end = current_sequence.find(rev_comp(reverse_sequence),start)
                end = current_sequence.find(rev_comp(reverse_sequence))
                if -1 == end:
                    return None
                if not (start < end):
                    return None
                filtered = current_record[:end]
                filtered = filtered[start+len(forward_sequence):]
                return filtered

            with gzip.open(str(merged_input_path), "rt") as infastq, gzip.open(str(output_path), "wt") as outfastq:
                for record in SeqIO.parse(infastq, "fastq"):
                    for f_primer_seq in unfold_primer(primer.filter_forward):
                        f_primer_seq_rev_comp = rev_comp(f_primer_seq)
                        # todo: if Fprimseq not in rec.seq and rev_comp(Fprimseq) not in rec.seq: continue
                        # todo: (cont.) else, do what?
                        # fixme: is it intentional that we stop at the first occurrence of a sequence and do not check
                        # fixme: (cont.) if there are more than one occurrence of either of the sequences?
                        # fixme: (cont.) we will also never check the other way round:
                        # fixme: (cont.) if forward in seq, never check reverse in seq
                        for r_primer_seq in unfold_primer(primer.filter_reverse):
                            # filtered_record = filter_primer(f_primer_seq, r_primer_seq, record)
                            # if None is not filtered_record:
                            #     SeqIO.write(filtered_record, outfastq, "fastq")
                            #     break
                            # # else try other direction:
                            # filtered_record = filter_primer(r_primer_seq, f_primer_seq, record)
                            # if None is not filtered_record:
                            #     SeqIO.write(filtered_record, outfastq, "fastq")
                            #     break

                            # replace loop code with commented code above if it does the same: i.e. no assertion errors
                            r_primer_seq_rev_comp = rev_comp(r_primer_seq)
                            sequence = record.seq  # type: Seq
                            if f_primer_seq in sequence and r_primer_seq_rev_comp in sequence:
                                filtered_record = filter_primer(f_primer_seq, r_primer_seq, record)
                                f_position = sequence.find(f_primer_seq)
                                r_position = sequence.find(r_primer_seq_rev_comp)
                                if f_position < r_position:
                                    record = record[:int(r_position)]
                                    record = record[int(f_position) + len(f_primer_seq):]
                                    assert str(record.seq) == str(filtered_record.seq)
                                    SeqIO.write(record, outfastq, "fastq")
                                    break
                            elif r_primer_seq in sequence and f_primer_seq_rev_comp in sequence:
                                filtered_record = filter_primer(r_primer_seq, f_primer_seq, record)
                                r_position = sequence.find(r_primer_seq)
                                f_position = sequence.find(f_primer_seq_rev_comp)
                                if r_position < f_position:
                                    record = record[:int(f_position)]
                                    record = record[int(r_position) + len(r_primer_seq):]
                                    assert str(record.seq) == str(filtered_record.seq)
                                    SeqIO.write(record, outfastq, "fastq")
                                    break

        print('Saving primer filter results for {primer}'.format(primer=primer.name))
        filtered_primers = list(sub_folders.primerfiltered.iterdir())
        for index, merged_input_path in enumerate(filtered_primers):
            print_progress(index+1, len(filtered_primers), 'filtered results ' + primer.name, merged_input_path.name)
            assert ".fastq.gz" in merged_input_path.name
            indiv = merged_input_path.name.split(".fastq.gz")
            indiv = indiv[0]
            if (primer.name, indiv) in filtered_series:
                if 0 != filtered_series[(primer.name, indiv)]:
                    continue
            filtered_series[(primer.name, indiv)] = count_records(merged_input_path, 'fastq')

    monitoring_df['PRIMER_FILTERED'] = filtered_series
    monitoring_df.to_csv(config.folders.root / 'mon.csv')
    for primer in config.primers:
        sub_folders = primer.folders.qualityfilter
        filtered_primer_files = sorted_natural(sub_folders.primerfiltered.iterdir())  # type: List[Path]
        for index, fastqfile in enumerate(filtered_primer_files):
            fastafile = sub_folders.highquality / fastqfile.name.replace(".fastq.gz", ".fasta.gz")
            print_progress(index+1, len(filtered_primer_files), 'quality-filtered ' + primer.name, fastqfile.name)
            if fastafile.is_file():
                continue
            with gzip.open(str(fastqfile), "rt") as infastq, gzip.open(str(fastafile), "wt") as outfasta:
                for record in SeqIO.parse(infastq, "fastq"):
                    quality = record.letter_annotations["phred_quality"]
                    if filter_quality(quality, primer.filter_p, primer.filter_q) == 1:
                        SeqIO.write(record, outfasta, "fasta")

        print('Saving quality control results for {primer}'.format(primer=primer.name))
        for qcedfile in sub_folders.highquality.iterdir():
            assert ".fasta.gz" in qcedfile.name
            indiv = qcedfile.name.split(".fasta.gz")
            indiv = indiv[0]
            if (primer.name, indiv) in qc_filtered_series:
                if 0 != qc_filtered_series[(primer.name, indiv)]:
                    continue
            with gzip.open(str(qcedfile), "rt") as infasta:
                k = len(list(SeqIO.parse(infasta, "fasta")))
                qc_filtered_series[(primer.name, indiv)] = k

    monitoring_df['QC_FILTERED'] = qc_filtered_series
    monitoring_df.to_csv(config.folders.root / 'mon.csv')


def collapse_sequences(config, monitoring_df):  # type: (AcaciaConfig, pd.DataFrame) -> None
    """
    (6) COLLAPSE: Collapses (dereplicates) all files and gets rid of all reads that appear only once in an individual.
    Files will be ready for chimera detection (VSEARCH) after this.
    :param config:
    :param monitoring_df:
    :return:
    """
    if not config.collapse_sequences:
        print('skipped step 6, collapse sequences')
        return
    if 'WITHOUT_SINGLETONS' in monitoring_df:
        singleton_series = monitoring_df['WITHOUT_SINGLETONS']
    else:
        singleton_series = pd.Series(index=monitoring_df.index, dtype='uint')
    print('step 6, collapse sequences')
    # todo: depends on step5
    for primer in config.primers:
        collapsed_output_folder = primer.folders.collapse.collapsed
        collapsed_output_folder.mkdir(parents=True, exist_ok=True)

        high_quality_folder = primer.folders.qualityfilter.highquality
        input_files = sorted_natural(high_quality_folder.iterdir())  # type: List[Path]
        for i, input_file in enumerate(input_files):
            print_progress(i+1, len(input_files), 'collapsed ' + primer.name, input_file.name)
            output_file = collapsed_output_folder / input_file.name
            proc = commands.run_vsearch_collapse(input_file, output_file)
            if output_file.is_file():
                continue
            err = [line.decode("utf-8") for line in proc.stderr.readlines()]
            if 0 != proc.returncode:
                print()
                for line in err:
                    if '' == line.strip():
                        continue
                    print(line)
        print(Bcolors.GREEN + "\nCollapsing done. Saving results for " + primer.name + Bcolors.END)
        gzipped_files = list(collapsed_output_folder.iterdir())
        for index, gzipped_file in enumerate(gzipped_files):
            print_progress(index+1, len(gzipped_files), 'save result ' + primer.name, gzipped_file.name)
            k = 0
            assert '.fasta' in gzipped_file.name
            indiv = gzipped_file.name.split(".fasta")[0]
            if (primer.name, indiv) in singleton_series:
                if 0 != singleton_series[(primer.name, indiv)]:
                    continue
            with gzip.open(str(gzipped_file), "rt") as infasta:
                for record in SeqIO.parse(infasta, "fasta"):
                    name = record.id
                    name = name.replace("=", ";")
                    name = name.split(";")
                    size = int(name[2])
                    k += size
            singleton_series[(primer.name, indiv)] = k
    monitoring_df['WITHOUT_SINGLETONS'] = singleton_series
    monitoring_df.to_csv(str(config.folders.root / 'mon.csv'))


def filter_chimeras(config, monitoring_df):  # type: (AcaciaConfig, pd.DataFrame) -> None
    """
    (7) Runs VSEARCH for detecting and filtering chimeras.
    #7: This will now identify and remove chimeras from your files.
    :param config:
    :param monitoring_df:
    :return:
    """
    # todo: this is only valid if collapsing (6) took place
    if not config.collapse_sequences:
        print('skipped chimera filtering, no collapsing performed')
        return
    singleton_series = monitoring_df['WITHOUT_SINGLETONS']
    if 'NON_CHIMERIC' in monitoring_df:
        non_chimeric_series = monitoring_df['NON_CHIMERIC']
    else:
        non_chimeric_series = pd.Series(index=monitoring_df.index, dtype='uint')
    for primer in config.primers:
        sub_folders = primer.folders.chimeras
        vsearchresults_folder = sub_folders.vsearchresults
        preliminary_folder = sub_folders.preliminary
        nonchimeric_folder = sub_folders.nonchimeric
        for subfolder in [vsearchresults_folder, preliminary_folder, nonchimeric_folder]:
            subfolder.mkdir(parents=True, exist_ok=True)

        collapsed_files = sorted_natural(primer.folders.collapse.collapsed.iterdir())

        for i, collapsed_file in enumerate(collapsed_files):
            print_progress(i, len(collapsed_files), 'Chimera detection ' + primer.name, collapsed_file.name)
            indiv = collapsed_file.name.split(".fasta")
            indiv = indiv[0]
            if singleton_series[(primer.name, indiv)] == 0:
                continue
            chimera_file = sub_folders.vsearchresults / (indiv + '.out')
            preliminary_file = sub_folders.preliminary / (indiv + '.fasta')
            if chimera_file.is_file() and preliminary_file.is_file():
                continue
            proc = commands.run_vsearch_chimera(collapsed_file, chimera_file, preliminary_file)
            err = [line.decode("utf-8") for line in proc.stderr.readlines()]
            proc.wait()
            if 0 != proc.returncode:
                print()
                for line in err:
                    if '' == line.strip():
                        continue
                    print(line)
        preliminary_files = sorted_natural(sub_folders.preliminary.iterdir())  # type: List[Path]
        columns = []
        for j in range(18):
            columns.append("u" + str(j + 1))
        for i, preliminary_file in enumerate(preliminary_files):
            indiv = preliminary_file.name.split(".fasta")
            indiv = indiv[0]
            chimera_file = sub_folders.vsearchresults / (indiv + '.out')
            if chimera_file.stat().st_size == 0:
                continue
            vsearchoutput_df = pd.read_csv(str(chimera_file), sep="\t", header=None, engine="python")
            vsearchoutput_df.columns = columns
            vsearchoutput_df_slim = vsearchoutput_df[(vsearchoutput_df.u6 == "100.0") & (vsearchoutput_df.u18 == "N") & (vsearchoutput_df.u1 >= 0.05)]
            shitlist = list(vsearchoutput_df_slim.u2)
            out_file = sub_folders.nonchimeric / preliminary_file.name
            with open(str(preliminary_file), "rt") as infasta, open(str(out_file), "wt") as outfasta:
                for record in SeqIO.parse(infasta, "fasta"):
                    if record.id not in shitlist:
                        SeqIO.write(record, outfasta, "fasta")

        print(Bcolors.GREEN + "\nChimera detection and filter done. Saving results for " + primer.name + Bcolors.END)
        for non_chimeric_file in sub_folders.nonchimeric.iterdir():

            assert '.fasta' in non_chimeric_file.name
            indiv = non_chimeric_file.name.split(".fasta")
            indiv = indiv[0]
            if (primer.name, indiv) in non_chimeric_series:
                if 0 != non_chimeric_series[(primer.name, indiv)]:
                    continue
            non_chimeric_series[(primer.name, indiv)] = fasta_sum_size(non_chimeric_file)

    monitoring_df['NON_CHIMERIC'] = non_chimeric_series
    monitoring_df.to_csv(config.folders.root / 'mon.csv')


def blast_sequences(config, monitoring_df):  # type: (AcaciaConfig, pd.DataFrame) -> None
    """
    (8) This will now BLAST your sequences against a local database.
    :param config:
    :param monitoring_df:
    :return:
    """
    # depends on collapse/chimera filter
    if not config.collapse_sequences:
        print('no collapsing took place, skipping sequence BLASTing')
        return
    if 'BLASTED' in monitoring_df:
        blasted_series = monitoring_df['BLASTED']
    else:
        blasted_series = pd.Series(index=monitoring_df.index, dtype='uint')
    for primer in config.primers:
        sub_folders = primer.folders.blast
        for sub_folder in (sub_folders.blastclean, sub_folders.blastresults, sub_folders.db):
            sub_folder.mkdir(exist_ok=True, parents=True)
        # (8.1.) set up database
        db_output = sub_folders.db / primer.name
        process = commands.run_makeblastdb(config.blast_reference_file, db_output)
        for line in process.stdout.readlines():
            print(line.decode("utf-8"),)
        iterator = primer.folders.chimeras.nonchimeric.iterdir()
        non_chimeric_files = sorted_natural(iterator)  # type: List[Path]
        # (8.2.) BLAST
        for i, non_chimeric_file in enumerate(non_chimeric_files):
            print_progress(i + 1, len(non_chimeric_files), 'blasting ' + primer.name, non_chimeric_file.name)
            blast_output = sub_folders.blastresults / non_chimeric_file.name.rstrip(".fasta")
            if blast_output.is_file():
                continue
            proc = commands.run_blastn(db_output, non_chimeric_file, blast_output)
            for line in proc.stdout.readlines():
                print(line.decode("utf-8"),)
        # (8.3.) filters blast results
        for i, non_chimeric_file in enumerate(non_chimeric_files):
            print_progress(i + 1, len(non_chimeric_files), 'filtering ' + primer.name, non_chimeric_file.name)
            clean_output = sub_folders.blastclean / non_chimeric_file.name.rstrip(".fasta")
            if clean_output.is_file():
                continue
            blast_output = sub_folders.blastresults / non_chimeric_file.name.rstrip(".fasta")
            threshold = 1e-20
            with open(str(blast_output)) as blastresultfile, \
                    open(str(non_chimeric_file), "rt") as infasta, \
                    open(str(clean_output), "wt") as outfasta:
                blastdict = {}
                for line in blastresultfile:
                    line = line.rstrip("\n")
                    line = line.replace(";size=", ",")
                    line = line.replace(";,", ",")
                    line = line.split(",")
                    identifier = line[0]
                    size = int(line[1])
                    evalue = float(line[2])
                    if evalue < threshold and identifier not in blastdict:
                        blastdict[identifier] = size
                for record in SeqIO.parse(infasta, "fasta"):
                    ident = record.id.split(";")[0]
                    if ident in blastdict:
                        SeqIO.write(record, outfasta, "fasta")

        print(Bcolors.GREEN + "\nBLAST filter done. Saving results for " + primer.name + Bcolors.END)
        cleaned_files = list(sub_folders.blastclean.iterdir())
        for index, cleaned_file in enumerate(cleaned_files):
            print_progress(index+1, len(cleaned_files), 'blasted results ' + primer.name, cleaned_file.name)

            # todo: this assert fails, should the blast outputs have the .fasta ext?
            # assert '.fasta' in cleaned_file.name
            indiv = cleaned_file.name.split(".fasta")[0]
            if (primer.name, indiv) in blasted_series:
                if 0 != blasted_series[(primer.name, indiv)]:
                    continue
            blasted_series[(primer.name, indiv)] = fasta_sum_size(cleaned_file)
    monitoring_df['BLASTED'] = blasted_series
    monitoring_df.to_csv(config.folders.root / 'mon.csv')


def align_fasta(config):  # type: (AcaciaConfig) -> None
    """
    (9) Creates and aligns pooled fasta files using MAFFT.
    Extract sequences from alignment in order to create individual aligned files.
    :param config:
    :return:
    """
    # depends on collapse-chimera-blasting
    if not config.collapse_sequences:
        print('no collapsing took place, skipping sequence alignment')
        return

    for primer in config.primers:
        sub_folders = primer.folders.align
        for sub_folder in (sub_folders.aligned, sub_folders.pooled):
            sub_folder.mkdir(exist_ok=True, parents=True)
        cleaned_blast_files = sorted_natural(primer.folders.blast.blastclean.iterdir())  # type: List[Path]
        pooled_output_file = sub_folders.pooled / 'pooled.fasta'
        if pooled_output_file.is_file():
            continue
        with open(str(pooled_output_file), "wt") as outfasta:
            for i, cleaned_blast_file in enumerate(cleaned_blast_files):
                with open(str(cleaned_blast_file), "rt") as infasta:
                    indiv = cleaned_blast_file.name.rstrip(".fasta")
                    for record in SeqIO.parse(infasta, "fasta"):
                        name = record.name
                        identifier = name.replace("=", ";")
                        identifier = identifier.split(";")
                        size = int(identifier[2])
                        record.id = str(indiv) + "_" + str(size)
                        record.name = record.description = record.id
                        SeqIO.write(record, outfasta, "fasta")
        aligned_output_file = pooled_output_file.parent / "pooled_aligned.fasta"
        commands.run_mafft(pooled_output_file, aligned_output_file)
        print(Bcolors.GREEN + "Alignment done for " + primer.name + Bcolors.END)
        id_list = []
        for index, cleaned_blast_file in enumerate(cleaned_blast_files):
            print_progress(index+1, len(cleaned_blast_files), 'getting aligned ids ' + primer.name, cleaned_blast_file.name)
            i = 0
            with open(str(cleaned_blast_file), "rt") as infasta:
                # we only want ids of files that contain more than one line
                for _ in SeqIO.parse(infasta, "fasta"):
                    i += 1
                    if i > 1:
                        id_list.append(cleaned_blast_file.name.rstrip(".fasta"))
                        break
        for indiv_id in id_list:
            input_file_path = sub_folders.pooled / 'pooled_aligned.fasta'
            output_file_path = sub_folders.aligned / (indiv_id + '.fasta')
            with open(str(input_file_path), "rt") as infasta, open(str(output_file_path), "wt") as outfasta:
                for record in SeqIO.parse(infasta, "fasta"):
                    identifier = record.id.split("_")[0]
                    if identifier == indiv_id:
                        SeqIO.write(record, outfasta, "fasta")
        print(Bcolors.GREEN + "\nDone." + Bcolors.END)


def convert_for_oligotype(config):  # type: (AcaciaConfig) -> None
    """
    (10) Converts the aligned files to proper format for entropy & oligotype, by expanding them.
    :param config:
    :return:
    """
    # depends on collapse-chimera-blasting-alignment
    if not config.collapse_sequences:
        print('no collapsing took place, skipping converting alignment/expansion for oligotype')
        return

    def convert(in_path, out_path):  # type: (Path, Path) -> None
        if out_path.is_file():
            return
        with open(str(in_path), "rt") as infasta, open(str(out_path), "wt") as outfasta:
            for record in SeqIO.parse(infasta, "fasta"):
                identifier = record.id.split("_")
                size = int(identifier[1])
                for i in range(size):
                    SeqIO.write(record, outfasta, "fasta")

    for primer in config.primers:
        sub_folders = primer.folders.expand
        sub_folders.expanded.mkdir(exist_ok=True, parents=True)
        align_sub_folders = primer.folders.align
        for aligned_input in align_sub_folders.aligned.iterdir():
            convert(aligned_input, sub_folders.expanded / aligned_input.name)
        convert(align_sub_folders.pooled / 'pooled_aligned.fasta', sub_folders.expanded / 'pooled.fasta')
        print(Bcolors.GREEN + "\nExpansions done for " + primer.name + Bcolors.END)


def analyze_entropy(config):  # type: (AcaciaConfig) -> None
    """
    (11) This will run the entropy analyses
    :param config:
    :return:
    """
    # depends on collapse-chimera-blasting-alignment-expansion
    if not config.collapse_sequences:
        print('no collapsing took place, skipping entropy analysis')
        return

    for primer in config.primers:
        sub_folders = primer.folders.entropy
        sub_folders.results.mkdir(exist_ok=True, parents=True)
        expanded_folder = primer.folders.expand.expanded
        expanded_files = sorted_natural(expanded_folder.glob('*.fasta'))  # type: List[Path]
        for i, expanded_file in enumerate(expanded_files):
            result_file = sub_folders.results / (expanded_file.name + '-ENTROPY')
            if result_file.is_file():
                continue
            proc = commands.run_entropy_analysis(expanded_file)
            errs = [line.decode('utf-8') for line in proc.stderr.readlines()]
            proc.wait()
            if 0 != proc.returncode:
                print('\n', *errs)

            print_progress(i+1, len(expanded_files), 'entropy analysis ' + primer.name, expanded_file.name)
        for expanded_file in expanded_folder.glob('*ENTROPY*'):
            expanded_file.rename(sub_folders.results / expanded_file.name)
        print(Bcolors.GREEN + "\nEntropy analysis finished for " + primer.name + Bcolors.END)


def oligotype_analysis(config, monitoring_df):  # type: (AcaciaConfig, pd.DataFrame) -> None
    """
    (12) run the oligotype analyses
    :param config:
    :param monitoring_df:
    :return:
    """
    # depends on collapse-chimera-blasting-alignment-expansion-entropy
    if not config.collapse_sequences:
        print('no collapsing took place, skipping oligotype analysis')
        return

    for primer in config.primers:
        sub_folders = primer.folders.oligotype
        sub_folders.results.mkdir(exist_ok=True, parents=True)

        # (12.1) finds the right components based on the individual entropy files
        entropy_threshold = 0.15
        components_list = []

        for entropy_path in primer.folders.entropy.results.glob('*-ENTROPY'):
            if entropy_path.name.startswith('pooled'):
                continue
            with open(str(entropy_path)) as entropyfile:
                for line in entropyfile:
                    line = line.rstrip("\n")
                    line = line.split("\t")
                    component = line[0]
                    entropy = float(line[1])
                    if entropy >= entropy_threshold:
                        if component not in components_list:
                            components_list.append(component)
        components_list = sorted_natural(components_list)
        selected_components = ",".join(components_list)

        # (12.2) runs oligotype on the pooled file only, using the components list
        series = monitoring_df['BLASTED'][primer.name]  # pd.Series
        totalnrofreads = series.sum()
        # totalnrofreads = sum(list(monitoring_df.BLASTED[monitoring_df.PRIMER == primer]))
        file_count = len(list(primer.folders.blast.blastclean.iterdir()))
        if 0 == file_count:
            print('Oligotype Analysis skipped for ' + primer.name + ', no input files')
            continue
        min_actual_abundance = 0.1 * totalnrofreads / file_count
        min_actual_abundance = int(round(min_actual_abundance, 0))
        print("\nPerforming oligotype analysis on the pooled file of " + primer.name)
        alignment_file = primer.folders.expand.expanded / 'pooled.fasta'
        entropy_file = primer.folders.entropy.results / 'pooled.fasta-ENTROPY'
        output_folder = sub_folders.results / 'pooled'
        proc = commands.run_oligotype(
            alignment_file,
            entropy_file,
            output_folder,
            selected_components,
            min_actual_abundance
        )
        for line in proc.stdout.readlines():
            print(line,)
        print(Bcolors.GREEN + "\nOligotype analysis finished for " + primer.name + Bcolors.END)


def extract_alleles(config, monitoring_df):  # type: (AcaciaConfig, pd.DataFrame) -> None
    """
    (13) filters out alleles and writes the report files
    This will now extract the alleles and write the reports for you.
    It will also name the alleles in decreasing order of abundance.
    :param config:
    :param monitoring_df:
    :return:
    """
    # depends on collapse-chimera-blasting-alignment-expansion-oligotype
    if not config.collapse_sequences:
        print('no collapsing took place, skipping allele extraction and report generation')
        return

    def calculate_abundances(oligo_cat):
        new_abundances = {}
        current_alleles = list(oligo_cat.SEQ)
        uniq_alleles = list((set(oligo_cat.SEQ)))
        for uniq_allele in uniq_alleles:
            allele_count = current_alleles.count(uniq_allele)
            new_abundances[uniq_allele] = allele_count

        if any([p.remove_singletons for p in config.primers]):
            print("Removing singletons.")
            removed_singletons = 0
            for uniq_allele in new_abundances:
                if new_abundances[uniq_allele] == 1:
                    removed_singletons += 1
                    oligo_cat = oligo_cat[new_oligo_catalogue.SEQ != uniq_allele]
            print("This step removed", removed_singletons, "singletons.")
            uniq_allele_count = oligo_cat.SEQ.nunique()
            print("There are now", uniq_allele_count, "alleles among all individuals.")
        return new_abundances

    knownallelenames_dict = {}
    for primer in config.primers:
        if None is primer.known_alleles or 'None' == primer.known_alleles:
            continue
        with gzip.open(str(primer.known_alleles)) as infasta:
            for name, seq in infasta:
                knownallelenames_dict[seq] = name
                knownallelenames_dict[rev_comp(seq)] = name
        if len(knownallelenames_dict) > 0:
            print(Bcolors.GREEN + "\nA fasta file containing " + str(
                int(len(knownallelenames_dict) / 2)) + " allele names and sequences was recognized." + Bcolors.END)
        else:
            print(Bcolors.RED + "\nA fasta file was indicated, but no sequences were recognized." + Bcolors.END)
        # min_nrofalleles = 1. # future
        # max_nrofalleles = 99. # future
        # nrofloci = not set. # future

    """
    (13.2) looks into oligotype results and creates master dataframe with 
    a: oligos, b: ids, c: counts, d: sequences and e: aligned sequences
    """
    for primer in config.primers:
        oligo_results_folder = primer.folders.oligotype.results
        tsv_path = oligo_results_folder / 'pooled' / 'ENVIRONMENT.txt'
        representatives_path = oligo_results_folder / 'pooled' / 'OLIGO-REPRESENTATIVES.fasta'
        if not tsv_path.is_file():
            print(str(tsv_path))
            print("The file ENVIRONMENT.txt could not be found. "
                  "You might want to re-run oligotype for primer: " + primer.name)
            continue
        if not representatives_path.is_file():
            print("The file OLIGO-REPRESENTATIVES.fasta could not be found. "
                  "You might want to re-run oligotype for primer: " + primer.name)
            continue
        oligo_catalogue = pd.read_csv(str(tsv_path), sep="\t", header=None, engine="python")
        oligo_catalogue.columns = ["OLIGO", "ID", "OLIGOCOUNT"]
        oligos_n_sequences_dict = {}
        oligos_n_sequences_withgaps_dict = {}
        with open(str(representatives_path), "rt") as representatives:
            for record in SeqIO.parse(representatives, "fasta"):
                oligo = record.id
                seq = str(record.seq)
                oligos_n_sequences_dict[oligo] = seq.replace("-", "")
                oligos_n_sequences_withgaps_dict[oligo] = seq
        oligosandseqs = pd.DataFrame(oligos_n_sequences_dict.items(), columns=["OLIGO", "SEQ"])
        oligosandseqs_withgaps = pd.DataFrame(oligos_n_sequences_withgaps_dict.items(),
                                              columns=["OLIGO", "SEQ_ALIGNED"])
        oligo_catalogue = pd.merge(oligo_catalogue, oligosandseqs, on="OLIGO")
        oligo_catalogue = pd.merge(oligo_catalogue, oligosandseqs_withgaps, on="OLIGO")

        numberofuniquealleles = oligo_catalogue.SEQ.nunique()
        print("The workflow produced", numberofuniquealleles, "alleles so far, considering all IDs.")

        # (13.3) builds dictionary with (preliminary) allele abundances. Remove singletons if wished.
        abundances = calculate_abundances(oligo_catalogue)

        abundances_df = pd.DataFrame(abundances.items(), columns=["SEQ", "ABUNDANCE"])

        # (13.4) looks at each individual separately
        abs_nor = primer.abs_nor
        low_por = primer.low_por
        new_oligo_catalogue = pd.DataFrame(columns=["OLIGO", "ID", "OLIGOCOUNT", "SEQ", "SEQ_ALIGNED"])
        indivs = list(set(oligo_catalogue.ID))
        for indiv in indivs:
            new_id_df = oligo_catalogue[oligo_catalogue.ID == indiv]
            new_id_df = new_id_df.sort_values(by=["OLIGOCOUNT"], ascending=False)
            # apply the abs_nor filter
            new_id_df = new_id_df[new_id_df.OLIGOCOUNT > abs_nor]
            localalleles = list(new_id_df.SEQ_ALIGNED)
            """
            filer out artifacts that differ from their "source" by
            only one nucleotide and that can be identified by frequency
            """
            for yelem, xelem in itertools.product(localalleles, localalleles):
                if localalleles.index(yelem) >= localalleles.index(xelem):
                    continue
                lowercount = new_id_df.OLIGOCOUNT[new_id_df.SEQ_ALIGNED == xelem]
                if len(lowercount) == 0:
                    continue
                proportion = lowercount.iloc[0]/sum(new_id_df.OLIGOCOUNT)
                if proportion < low_por:
                    new_id_df = new_id_df[new_id_df.SEQ_ALIGNED != xelem]  # apply the low_por filter
                    continue
                nrdiff = sequence_compare(xelem, yelem)
                if nrdiff != 1:
                    continue
                count_a = new_id_df.OLIGOCOUNT[new_id_df.SEQ_ALIGNED == yelem]
                count_b = new_id_df.OLIGOCOUNT[new_id_df.SEQ_ALIGNED == xelem]
                if len(count_a) == 0 or len(count_b) == 0:
                    continue
                if count_a.iloc[0] / count_b.iloc[0] <= 10:
                    continue
                if count_a.iloc[0]/count_b.iloc[0] <= 10:
                    continue
                # count of bigger allele is more than 10times the count of smaller allele IN THE INDIVIDUAL
                # count the oligos representing bigger allele in the pop
                count_biggerallele = int(sum(oligo_catalogue.OLIGOCOUNT[oligo_catalogue.SEQ_ALIGNED == yelem]))
                # count the oligos representing smaller allele in the pop
                count_smallerallele = int(sum(oligo_catalogue.OLIGOCOUNT[oligo_catalogue.SEQ_ALIGNED == xelem]))
                # count of bigger allele is more than 10times the count of smaller allele IN THE POPULATION.
                if count_biggerallele/count_smallerallele <= 10:
                    continue
                if abundances[xelem.replace("-", "")] >= 0.01 * sum(list(abundances_df["ABUNDANCE"])):
                    continue
                # now we have enough evidence to discard that smaller allele.
                new_id_df = new_id_df[new_id_df.SEQ_ALIGNED != xelem]

            potential_chimeras = {}
            for index1, row1 in new_id_df.iterrows():
                for index2, row2 in new_id_df.iterrows():
                    if not (row1["OLIGOCOUNT"] / row2["OLIGOCOUNT"] > 4):
                        continue
                    if index2 not in potential_chimeras:
                        potential_chimeras[index2] = [index1]
                    else:
                        potential_chimeras[index2].append(index1)

            for key, values in potential_chimeras.items():
                if not len(values) > 1:
                    continue
                try:
                    for elem in values:
                        if key not in list(new_id_df.index.values):
                            continue
                        # get_value() deprecated. Replace though .at[] or .iat[]
                        child = new_id_df.get_value(key, "SEQ_ALIGNED")
                        # get_value() deprecated. Replace though .at[] or .iat[]
                        parent = new_id_df.get_value(elem, "SEQ_ALIGNED")
                        if not (sequence_compare(child, parent) == sequence_compare(child[-5:], parent[-5:])):
                            continue
                        # all differences are concentrated in the last 5 bases only!
                        for item in potential_chimeras[key]:
                            # start looking for the other parent
                            # get_value() deprecated. Replace though .at[] or .iat[]
                            other_parent = new_id_df.get_value(item, "SEQ_ALIGNED")
                            if not (other_parent != parent):
                                continue
                            if not (child == parent[:-5] + other_parent[-5:]):
                                continue
                            # if this is true than we have found a late chimera
                            new_id_df = new_id_df[new_id_df.SEQ_ALIGNED != child]
                except:
                    # todo: what do we do here? some feedback to the user?
                    # todo: most important: what exceptions are expected?
                    pass

            new_oligo_catalogue = new_oligo_catalogue.append(new_id_df, ignore_index=True)

        """
        (13.5) removes very "thin" IDs. 
        IDs need to have reached this point with at least 50 reads in order to make alleles
        """
        indivs = list(set(new_oligo_catalogue.ID))
        for indiv in indivs:
            sumofallreads = sum(new_oligo_catalogue.OLIGOCOUNT[new_oligo_catalogue.ID == indiv])
            if sumofallreads < 50:
                new_oligo_catalogue = new_oligo_catalogue[new_oligo_catalogue.ID != indiv]

        print("\n\nWe are now at", new_oligo_catalogue.SEQ.nunique(), "alleles.")
        abundances = calculate_abundances(new_oligo_catalogue)
        abundances_df = pd.DataFrame(abundances.items(), columns=["SEQ", "ABUNDANCE"])
        abundances_df["ALLELE_NAMES"] = "NA"

        # (13.6) assigns new allele names based on abundance
        # (but consider known allele names, in case a fasta file was indicated before)
        abundances_df = abundances_df.sort_values(by=["ABUNDANCE"], ascending=False)
        abundances_df = abundances_df.reset_index(drop=True)
        i = 1
        for index, row in abundances_df.iterrows():
            if row["SEQ"] in knownallelenames_dict:
                abundances_df.loc[abundances_df.SEQ == row["SEQ"], "ALLELE_NAMES"] = knownallelenames_dict[row["SEQ"]]
            else:
                alleleprefix = primer.alleles_locus + '*'
                abundances_df.loc[abundances_df.SEQ == row["SEQ"], "ALLELE_NAMES"] = alleleprefix + "{:02d}".format(i)
                i += 1

        # (13.7) saves data into monitoring_df
        allele_count_series = pd.Series(index=monitoring_df.index, dtype='uint')
        for indiv in list(set(new_oligo_catalogue.ID)):
            allele_count_series[(primer.name, indiv)] = len(new_oligo_catalogue[new_oligo_catalogue.ID == indiv])
        monitoring_df['NR_OF_ALLELES'] = allele_count_series
        monitoring_df = monitoring_df.fillna(0)

        # (13.8) saves the pipeline report
        monitor_out_path = config.folders.reports / primer.name / 'pipelinereport.csv'
        monitor_out_path.parent.mkdir(parents=True, exist_ok=True)
        monitoring_df.to_csv(str(monitor_out_path), encoding="utf-8", index=True)

        # (13.9) saves detailed allele report
        allele_report_xl = new_oligo_catalogue.copy()
        allele_report_xl = allele_report_xl.drop(["OLIGO", "SEQ_ALIGNED"], axis=1)
        allele_report_xl["ALLELE"] = "NA"
        for index, row in allele_report_xl.iterrows():
            seq = row["SEQ"]
            name = abundances_df.ALLELE_NAMES[abundances_df.SEQ == seq]
            allele_report_xl.loc[allele_report_xl.SEQ == seq, "ALLELE"] = list(name)[0]
        allele_report_xl = allele_report_xl.rename(index=str, columns={"OLIGOCOUNT": "COUNT"})
        allele_report_xl = allele_report_xl[["ID", "ALLELE", "COUNT", "SEQ"]]
        allelereport_xl_path = config.folders.reports / primer.name / 'allelereport_XL.csv'
        allele_report_xl.to_csv(str(allelereport_xl_path), encoding="utf-8", index=False)

        # (13.10) saves the canonical allele report
        canonical_allele_report_path = config.folders.reports / primer.name / 'allelereport.csv'
        with open(str(canonical_allele_report_path), "w") as outfile:
            indivs = list(set(new_oligo_catalogue.ID))
            indivs = sorted_natural(indivs)
            print("This is an allele report prior to duplicate resolution. There are", len(indivs), "individuals here.", "\n", 2*(100 * "*"), file=outfile)
            print("\n", "Population report:", "\n", "Allele,Abundance,Sequence", file=outfile)
            for index, row in abundances_df.iterrows():
                print(str(row["ALLELE_NAMES"]) + "," + str(row["ABUNDANCE"]) + "," + str(row["SEQ"]), file=outfile)
            print("\n", 100*"*", "\n", file=outfile)
            print("Individual report:", "\n", "Individual,Nr_of_alleles,Alleles", file=outfile)
            numberofalleles = []
            for individual in indivs:
                individualallelelist = [allele for allele in list(allele_report_xl.ALLELE[allele_report_xl.ID == individual])]
                sorted_natural(individualallelelist)
                print(str(individual) + "," + str(len(individualallelelist)) + "," + ";".join(individualallelelist), file=outfile)
                numberofalleles.append(len(individualallelelist))
            print("\n", 100 * "*", "\n", "There are between", np.min(numberofalleles), "and", np.max(numberofalleles), "alleles in the individuals here. The mean is", np.mean(numberofalleles), "and the SD is " + str(np.std(numberofalleles)) + ".", end="", file=outfile)
        # (13.11) writes a fasta file with the alleles
        allele_fasta_path = config.folders.reports / primer.name / (primer.alleles_locus + '.fasta')
        with open(str(allele_fasta_path), "wt") as outfasta:
            for index, row in abundances_df.iterrows():
                record = SeqRecord(Seq(row["SEQ"]), row["ALLELE_NAMES"], '', '')
                SeqIO.write(record, outfasta, "fasta")

    print(Bcolors.GREEN + "\nThis is the end of the ACACIA pipeline. Your results are waiting for you in the folder " +
          str(config.folders.reports) + Bcolors.END)
    print(Bcolors.BLUE + ICON + Bcolors.END)


if __name__ == '__main__':
    try:
        main()
    except SystemExit as e:
        print('\nexiting', e.code)
