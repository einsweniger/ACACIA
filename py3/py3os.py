# For testing purposes, make sure the function is available when the C
# implementation exists.
def _fspath(path):
    """Return the path representation of a path-like object.

    If str or bytes is passed in, it is returned unchanged. Otherwise the
    os.PathLike interface is used to get the path representation. If the
    path representation is not str or bytes, TypeError is raised. If the
    provided path is not str, bytes, or os.PathLike, TypeError is raised.
    """
    if isinstance(path, (str, bytes)):
        return path

    # Work from the object's type to match method resolution of other magic
    # methods.
    path_type = type(path)
    try:
        path_repr = path_type.__fspath__(path)
    except AttributeError:
        if hasattr(path_type, '__fspath__'):
            raise
        else:
            raise TypeError("expected str, bytes or os.PathLike object, "
                            "not " + path_type.__name__)
    if isinstance(path_repr, (str, bytes)):
        return path_repr
    else:
        raise TypeError("expected {}.__fspath__() to return str or bytes, "
                        "not {}".format(path_type.__name__,
                                        type(path_repr).__name__))


class DirEntry(object):
    # no doc
    def inode(self, *args, **kwargs): # real signature unknown
        """ Return inode of the entry; cached per entry. """
        pass

    def is_dir(self, *args, **kwargs): # real signature unknown
        """ Return True if the entry is a directory; cached per entry. """
        pass

    def is_file(self, *args, **kwargs): # real signature unknown
        """ Return True if the entry is a file; cached per entry. """
        pass

    def is_symlink(self, *args, **kwargs): # real signature unknown
        """ Return True if the entry is a symbolic link; cached per entry. """
        pass

    def stat(self, *args, **kwargs): # real signature unknown
        """ Return stat_result object for the entry; cached per entry. """
        pass

    def __fspath__(self, *args, **kwargs): # real signature unknown
        """ Returns the path for the entry. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """the entry's base filename, relative to scandir() "path" argument"""

    path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """the entry's full path name; equivalent to os.path.join(scandir_path, entry.name)"""
