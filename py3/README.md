All files contained here are taken from the official cpython codebase.

The code was changed to work with python2.7 where necessary.

All code in here are is distributed under the licensing terms of the "PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2", 
which can be found within this folder.  