# ![ACACIA](logo/logo3web.png)

# ACACIA stands for "Allele CAlling proCedure for Illumina Amplicon sequencing data".

This script aims at extracting allele information out of raw paired-end Illumina sequencing data. It does so by denoising, filtering and clustering sequences.
ACACIA will call and name alleles out of your raw data quickly and precisely.

## Installation:

Due to [`Oligotyping`](http://merenlab.org/software/oligotyping), ACACIA currently runs in Python2.7. It requires a couple of python packages (notably [`Pandas`](https://pandas.pydata.org/) and [`Biopython`](https://biopython.org/)), as well as 6 pieces of third-party software (shown below). Due to constrains of some of these software, this pipeline runs preferentially in a 64bit, UNIX-like system.

I recommend following the instructions below to make sure all goes well:

#### 1. Setup a new Python2.7 virtual environment for acacia and install [`Anaconda`](https://anaconda.org/) in it:
Open a terminal and type:

```sh
conda create -n acacia python=2.7.16 anaconda
```

#### 2. Setup the [`Bioconda`](https://bioconda.github.io/) channel
First, activate the acacia virtual environment:

```sh
source activate acacia
```

Then, setup Bioconda by typing (one line at a time, in that order):

```sh
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```

#### 3. Install 6 third-party software:

Thanks to Bioconda, installing the tools [`Oligotyping`](http://merenlab.org/software/oligotyping/), [`FASTQC`](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/), [`FLASh`](https://ccb.jhu.edu/software/FLASH/), [`VSEARCH`](https://github.com/torognes/vsearch/), [`BLAST`](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download) and [`MAFFT`](https://mafft.cbrc.jp/alignment/software/) is extremely easy. Just type:

```sh
conda install -c bioconda oligotyping fastqc flash vsearch blast mafft
```
All other dependencies (Pandas and Biopython) will be installed automatically.

#### 4. Get ACACIA:

Start by downloading the code file by typing:
```sh
wget https://gitlab.com/psc_santos/ACACIA/blob/master/acacia.py
```
You should now move or copy `acacia.py` into a folder dedicated to ACACIA projects. Then, navigate to that folder and start ACACIA (remember to activate the acacia virtual environment fist!) by typing:

```sh
python acacia.py
```

## Finally, you will also need:
* Your paired-end raw Illumina FASTq files ([`example files here`](https://figshare.com/s/017ee3dae46edc656c08)).
* The names and exact sequences of your template-specific primers ([`example here`](https://figshare.com/s/9f40d994cd39a02e9998)).
* A fasta file with 100+ sequences related to those that you expect to have sequenced ([`here is an example`](https://figshare.com/s/3148ef72590db2c47acc)). This file will be used by ACACIA to setup a local BLAST database.

## Citing & Documentation:
ACACIA is described with detail [`in this paper.`](https://doi.org/10.1101/638288) The article has not yet been peer-reviewed, but you are welcome to cite it and also to comment!
